module Suav
  module Error
    
    class UnexpectedTypeError < StandardError
      def initialize(msg = 'You\'ve triggered a UnexpectedTypeError')
        super(msg)
      end
    end
    
  end
end