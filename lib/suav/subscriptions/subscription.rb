module Suav
  module Subscriptions
    class Subscription < ActiveRecord::Base

      belongs_to :subscribable, polymorphic: true
      validates :expires_at, presence: true

      def self.create_for(account, plan_id)
        account.subscriptions.find_or_create_by(plan_id: plan_id) do |s|
          s.expires_at = Date.today - 1.day
        end
      end

      def associated_plan
        @associated_plan ||= Suav::Subscriptions::Plan.new(
          plan_id, subscribable.class.const_get(:PLANS)[plan_id.to_sym]
        )
      end

      # Methods to handle subscription status

      def paid?
        !expired? && paid_at <= Date.today
      end

      def expired?
        expires_at < Date.today
      end

    end
  end
end
