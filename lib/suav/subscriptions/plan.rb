module Suav
  module Subscriptions
    class Plan

      attr_reader :id, :name, :price, :recurrence, :trial_period

      def initialize(id, attributes)
        @id = id.to_sym
        @name = attributes[:name]
        @price = attributes[:price]
        @recurrence = attributes[:recurrence]
        @trial_period = attributes[:trial_period]
      end

      def recurrence_days
        case recurrence
        when :weekly then 7
        when :monthly then 30
        when :annually then 365
        else
          raise StandardError, 'I can\'t understand this recurrence name.'
        end
      end

      def expiration_date_for(date)
        date + recurrence_days.days
      end

    end
  end
end
