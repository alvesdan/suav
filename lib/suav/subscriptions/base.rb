module Suav
  module Subscriptions
    module Base
      extend ActiveSupport::Concern

      included do
        def self.has_subscriptions
          raise StandardError, 'Could not find any subscription plan' unless self.const_defined?(:PLANS)
          class_eval do
            has_many :subscriptions, as: :subscribable, class_name: 'Suav::Subscriptions::Subscription'
          end
        end
      end

      def plan_types
        self.class.const_get(:PLANS)
      end

      def subscribe(id)
        raise StandardError, 'Could not find plan with given id' unless plan_types.has_key?(id)
        Subscription.create_for(self, id)
      end

    end
  end
end
