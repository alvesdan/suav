module Suav
  module Subscriptions

    autoload :Base, 'suav/subscriptions/base'
    autoload :Plan, 'suav/subscriptions/plan'
    autoload :Subscription, 'suav/subscriptions/subscription'

  end
end
