ENV["RAILS_ENV"] ||= 'test'

if ENV['COVERAGE'] == 'on'
  require 'simplecov'
  SimpleCov.start do
    add_filter "spec/"
  end
end

require File.expand_path('../dummy/config/environment', __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'database_cleaner'
require 'suav'
require 'pry'

# ['spec/support/**/*.rb', 'spec/lib/suav/**/*.rb'].each do |path|
#   Dir[Rails.root.join(path)].each {|f| require f }
# end

RSpec.configure do |config|

  config.use_transactional_fixtures = false
  config.infer_base_class_for_anonymous_controllers = false
  config.order = 'random'
  config.color_enabled = true

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end
  config.before(:each) do
    DatabaseCleaner.start
  end
  config.after(:each) do
    DatabaseCleaner.clean
  end

end
