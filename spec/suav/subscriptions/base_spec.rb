require 'spec_helper'

describe Suav::Subscriptions::Base do

  subject { Account.create(name: 'Test Account') }

  context :include do
    it 'should respond to #has_subscriptions' do
      expect(Account).to respond_to(:has_subscriptions)
    end
  end

  context :subscriptions do
    it 'should be kind of Suav::Subscriptions::Subscription' do
      expect(subject.subscriptions.new).to be_kind_of(Suav::Subscriptions::Subscription)
    end
  end

  context :subscribe do
    it 'should create subscription object' do
      Suav::Subscriptions::Subscription.should_receive(:create_for).with(subject, :standard)
      subject.subscribe(:standard)
    end
  end

end
