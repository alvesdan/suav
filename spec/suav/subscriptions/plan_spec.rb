require 'spec_helper'

describe Suav::Subscriptions::Plan do

  let(:plan_attributes) do
    {
      name: 'Standard',
      price: 9.90,
      recurrence: :monthly,
      trial_period: 30
    }
  end

  subject do
    Suav::Subscriptions::Plan.new(:standard, plan_attributes)
  end

  context :initialize do
    it 'should set instance variables' do
      expect(subject.id).to eq :standard
      expect(subject.name).to eq plan_attributes[:name]
      expect(subject.price).to eq plan_attributes[:price]
      expect(subject.recurrence).to eq plan_attributes[:recurrence]
      expect(subject.trial_period).to eq plan_attributes[:trial_period]
    end
  end

  context :recurrence_days do
    it 'should return 30 for monthly recurrence' do
      expect(subject.recurrence_days).to eq 30
    end
    it 'should return 7 for weekly recurrence' do
      subject.stub(:recurrence).and_return(:weekly)
      expect(subject.recurrence_days).to eq 7
    end
    it 'should return 365 for annually recurrence' do
      subject.stub(:recurrence).and_return(:annually)
      expect(subject.recurrence_days).to eq 365
    end
    it 'should rais exception when not in one of the known recurrences' do
      subject.stub(:recurrence).and_return(:unknown)
      expect { subject.recurrence_days }.to raise_error(StandardError)
    end
  end

  context :expiration_date_for do
    it 'should return 30 days after today' do
      expect(subject.expiration_date_for(Date.today)).to eq Date.today + 30.days
    end
  end

end
