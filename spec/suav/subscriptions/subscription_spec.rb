require 'spec_helper'

describe Suav::Subscriptions::Subscription do

  subject { Account.create(name: 'Test Account') }

  describe :validations do
    it 'should not allow to create same plan twice' do
      subject.subscribe(:standard)
      subject.subscribe(:standard)
      expect(subject.subscriptions.where('id IS NOT NULL').size).to eq 1
    end
    it 'should return created plan when trying to subscribe twice' do
      subscription = subject.subscribe(:standard)
      expect(subject.subscribe(:standard)).to eq subscription
    end
  end

  describe :associated_model do
    it 'should return associated model' do
      expect(subject.subscriptions.new.subscribable).to eq subject
    end
  end

  before do
    @subscription = subject.subscribe(:standard)
    @today = Date.today
  end

  describe :associated_plan do
    it 'should return standard plan' do
      expect(@subscription.associated_plan).to be_an_instance_of(Suav::Subscriptions::Plan)
    end
  end

  describe :paid? do
    [
      [true, Date.today - 1.day, false],
      [false, Date.today - 1.day, true],
      [false, Date.today + 1.day, false]
    ].each do |config|
      context "when expired is #{config[0]}, today is #{@today} and paid at is #{config[1]}" do
        it "should return #{config[2]}" do
          @subscription.stub(expired?: config[0])
          @subscription.stub(paid_at: config[1])
          expect(@subscription.paid?).to eq config[2]
        end
      end
    end
  end

  describe :expired? do
    before { @today = Date.today }
    it 'should return true when expires at is smaller than today' do
      @subscription.stub(expires_at: @today - 1.day)
      expect(@subscription.expired?).to be_true
    end
  end

end
