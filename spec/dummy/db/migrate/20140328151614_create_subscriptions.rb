class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :subscribable, polymorphic: true
      t.date :paid_at
      t.date :expires_at
      t.string :status, default: 'active'
      t.string :plan_id
    end
  end
end
