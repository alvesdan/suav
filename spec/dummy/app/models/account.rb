class Account < ActiveRecord::Base

  include Suav::Subscriptions::Base

  PLANS = {
    standard: {
      name: 'Standard',
      price: 9.90,
      recurrence: :monthly,
      trial_period: 30
    }
  }

  has_subscriptions

end
