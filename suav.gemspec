$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'suav/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|

  s.name = 'suav'
  s.version = Suav::VERSION
  s.authors = ['Daniel Alves']
  s.email = ['daniel@danielalves.me']
  s.homepage = ''
  s.summary = ''
  s.description = ''

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.add_dependency 'rails', '~> 4.0.4'

  s.add_development_dependency 'sqlite3'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'pry'

end
